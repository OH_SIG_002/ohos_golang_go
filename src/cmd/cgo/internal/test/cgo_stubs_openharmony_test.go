// Copyright 2024 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build openharmony

package cgotest

import "testing"

// Stubs for tests that fails to build on OpenHarmony
func test6997(t *testing.T) {}
